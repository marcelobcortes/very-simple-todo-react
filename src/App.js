import React, { Component } from 'react';
import List from './List';
import ListDone from './ListDone';
import shortid from 'shortid';

class Task {
  done= false;
  desc= '';
  id= 0;

  constructor (desc, id) {
    this.desc = desc;
    this.id = id;
  }

  toggleDone = () => {
    this.done = !this.done;
  }

  getDone =() => this.done;

  getDesc =() => this.desc;

  getId =() => this.id;
}
class App extends Component {

  state = {
    text: '',
    tasks: []
  }

  handleChange = event => {
    this.setState({text: event.target.value});
  }

  handleAdd = event => {
    const { tasks, text } = this.state;

    let task = new Task(text, shortid.generate());
    
    const new_state = tasks.concat(task);
    this.setState({tasks: new_state});
  }

  checkTask = id => {
    let {tasks} = this.state;

    tasks.forEach((value, index) => {
      if (value.getId() === id){
        tasks[index].toggleDone();
      }
    })

    this.setState({ tasks: tasks});
  }

  render() {
    const { text, tasks } = this.state;
    return (
      <div>
        <Container>
          <h1>ToDo</h1>
          <List tasks={tasks} check={this.checkTask} />
        </Container>
        <input 
          type="text"
          value={text}
          onChange={this.handleChange}
        />
        <button 
          onClick={this.handleAdd}
          disabled={!text.length}>
          Add</button>
        <br/>

        <Container>
          <h1>Done Tasks</h1>
          <ListDone tasks={tasks} />
        </Container>
      </div>
    );
  }
}

const Container = ({children}) => children;
  
export default App;