import React from 'react'

const List = ({tasks, check}) => (
    <ul>
        {
            !tasks.length ?
            `Lista em branco` :
            tasks.map((task)=>(
                <li key={task.getId()}>
                    <input 
                        type="checkbox" 
                        checked={task.getDone()}
                        onChange={()=> check(task.getId())}
                        />
                    {task.getDesc()}
                </li>
            ))
        }
    </ul>
);

export default List;