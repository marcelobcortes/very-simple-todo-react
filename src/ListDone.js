import React from 'react'

const ListDone = ({tasks}) => (
    <ul>
        {
            !tasks.length ?
            `Lista em branco` :
            tasks.map((task)=>
                task.getDone() ? <Task task={task}/> : ''
            )
        }
    </ul>
);

const Task = ({task}) => (
    <li key={task.getId()}>
        {task.getDesc()}
    </li>
)

export default ListDone;